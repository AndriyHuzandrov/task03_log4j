package com.nexus6.task03_Log4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Demo {
  private static final Logger topLog = LogManager.getLogger(Demo.class);
  public static void main(String[] args) {

    StackEmul st = new StackEmul();
    topLog.trace("top trace msg");
    topLog.debug("top debug msg");
    topLog.info("top info msg");
    topLog.warn("top warn msg");
    topLog.error("top err msg");
    topLog.fatal("top fatal msg");

  }
}
