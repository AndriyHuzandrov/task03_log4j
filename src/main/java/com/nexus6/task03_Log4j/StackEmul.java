package com.nexus6.task03_Log4j;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StackEmul {
  private static final Logger log = LogManager.getLogger(StackEmul.class);

  StackEmul() {
    log.trace("Stack object created");
  }
  void insData(int d) {

  }

}
